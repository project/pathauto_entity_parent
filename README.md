CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration

INTRODUCTION
------------

The Pathauto entity parent module allows to nest node paths N times.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/pathauto_entity_parent

REQUIREMENTS
------------

This module requires the following modules:
* [Pathauto](https://www.drupal.org/project/pathauto)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.


CONFIGURATION
-------------

To configure the module, you must go to the administration page
(admin/config/search/parent) and select the content types where
you want the nesting field to appear.
