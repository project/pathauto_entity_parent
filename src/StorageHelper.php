<?php

namespace Drupal\pathauto_entity_parent;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;

/**
 * Manage operations on content types based on pathauto entity parent options.
 */
class StorageHelper {

  /**
   * Config factory interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * The config factory.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Database\Connection $connection
   *   Connection.
   */
  public function __construct(ConfigFactoryInterface $configFactory, Connection $connection) {
    $this->configFactory = $configFactory;
    $this->connection    = $connection;
  }

  /**
   * When a bundle is unchecked, we remove the value of the field in the nodes.
   *
   * @param array $bundles
   *   Bundles.
   */
  public function removeValueFromBundles(array $bundles): void {
    $this->connection->update('node_field_data')
      ->fields(['pathauto_entity_parent' => NULL])
      ->condition('type', $bundles, 'IN')
      ->isNotNull('pathauto_entity_parent')
      ->execute();
  }

}
