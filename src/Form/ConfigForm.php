<?php

namespace Drupal\pathauto_entity_parent\Form;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pathauto_entity_parent\StorageHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the configuration options for the module.
 */
final class ConfigForm extends ConfigFormBase {

  /**
   * Pathauto's entity parent settings id.
   */
  public const PATHAUTO_ENTITY_PARENT_SETTINGS = 'pathauto_entity_parent.settings';

  /**
   * Entity type bundle info interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * Entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Pathauto's entity parent storage helper.
   *
   * @var \Drupal\pathauto_entity_parent\StorageHelper
   */
  protected StorageHelper $storageHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static($container->get('config.factory'), $container->get('entity_type.bundle.info'), $container->get('entity_type.manager'), $container->get('pathauto_entity_parent.storage_helper'));
  }

  /**
   * PatternEditForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity manager service.
   * @param \Drupal\pathauto_entity_parent\StorageHelper $storageHelper
   *   Storage helper.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeBundleInfoInterface $entityTypeBundleInfo, EntityTypeManagerInterface $entityTypeManager, StorageHelper $storageHelper) {
    parent::__construct($config_factory);
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->entityTypeManager    = $entityTypeManager;
    $this->storageHelper        = $storageHelper;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      self::PATHAUTO_ENTITY_PARENT_SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'pathauto_entity_parent_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    // Get settings.
    $settings = $this->config(self::PATHAUTO_ENTITY_PARENT_SETTINGS);

    try {
      if ($entity_type = $this->entityTypeManager->getDefinition('node')) {
        if ($entity_type->hasKey('bundle') && $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type->id())) {
          $bundle_options = [];
          foreach ($bundles as $id => $info) {
            $bundle_options[$id] = $info['label'];
          }
          $form['bundles']   = [
            '#title'         => $entity_type->getBundleLabel(),
            '#type'          => 'checkboxes',
            '#options'       => $bundle_options,
            '#default_value' => $settings->get('bundles') ?? [],
            '#description'   => $this->t('Check to which types this pattern should be applied.'),
          ];
        }
      }
    }
    catch (PluginNotFoundException $e) {
      // Unhandled exception message.
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $settings = $this->configFactory->getEditable(self::PATHAUTO_ENTITY_PARENT_SETTINGS);

    $available_bundles = $settings->get('bundles') ?? [];
    $removed           = array_diff($available_bundles, array_filter($form_state->getValue('bundles')));
    if (!empty($removed)) {
      $this->storageHelper->removeValueFromBundles($removed);
    }
    // Save configurations.
    $settings->set('bundles', array_filter($form_state->getValue('bundles')))->save();
  }

}
